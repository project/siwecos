# CONTENTS OF THIS FILE
-------------------------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


# INTRODUCTION
-------------------

SIWECOS is a project sponsored by the German government.
It provides a set of scanners for common website security vulnerabilities.
The report gives recommendations for securing the setup, e. g. by content security policy (CSP) headers.

 * For a full description of the module, visit the [project page](https://www.drupal.org/project/siwecos)
 * [Submit](https://www.drupal.org/project/issues/siwecos) bug reports and feature suggestions, or track changes


# REQUIREMENTS
--------------------

This module requires no modules outside of Drupal core but it requires an
external library to be installed in the libraries folder.

 * Download and extract the [jquery-circle-progress library](https://github.com/kottenator/jquery-circle-progress/archive/refs/tags/1.2.2.zip) to the `/libraries/jquery-circle-progress` folder.


# INSTALLATION
------------------

 * Install as you would normally install a contributed Drupal module. Visit [Siwecos releases](https://www.drupal.org/project/siwecos/releases) for further information.


# CONFIGURATION
--------------------------

* Register your account at the Siwecos project site and copy your credentials
  from there. (No data abuse, promised.)
* Enter email and password of your Siwecos account at
  `/admin/config/system/siwecos`
* Access your site's report at
  `/admin/reports/siwecos`

Please consider storing the credentials in your `settings.php` file for deployment:

```php
# The domain settings.
$settings['siwecos.settings']['domain'] = 'example.org';
$settings['siwecos.settings']['domain_token'] = 'ZieJ7ocaeGhuMohlie8le4lanai0looseo5eiSh0ch';

# The API credentials.
$settings['siwecos.settings']['email'] = 'user@example.org';
$settings['siwecos.settings']['password'] = 'ohnouDek5soec3Ju';
$settings['siwecos.settings']['api_token'] = 'ahph4dooFithi5pheif0og4naexe7Eeb5uizipahjaes7koos2raa7kah0ooR9ch';
```

> **Please note:**
> There's currently no support for `.htpasswd` protected or otherwise not publicly accessible sites.


# MAINTAINERS
---------------------

Current maintainers:

 * Stefan Auditor (sanduhrs) - https://www.drupal.org/u/sanduhrs
 * Carsten Logemann (C-Logemann) - https://www.drupal.org/u/C-Logemann
 * Meike Jung (hexabinaer) - https://www.drupal.org/u/hexabinaer
